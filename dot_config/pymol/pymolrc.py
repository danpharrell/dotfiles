from pathlib import Path
from pymol import cmd

FETCHES_DIR = Path("~/.cache/pymol/fetches").expanduser()
FETCHES_DIR.mkdir(exist_ok=True, parents=True)
cmd.set("fetch_path", str(FETCHES_DIR))

TABLEAU_10_COLORS = [
    "0x1F77B4",
    "0xFF7F0E",
    "0x2CA02C",
    "0xD62728",
    "0x9467BD",
    "0x8C564B",
    "0xE377C2",
    "0x7F7F7F",
    "0xBCBD22",
    "0x17BECF",
]


def recolor_tab10(sele: str = ""):
    """
    DESCRIPTION

        for every model, recolor 'modelname and {sele}' in tab10 colors

    ARGUMENTS

        sele = string: per-model atom selection
    """
    tab10_len = len(TABLEAU_10_COLORS)
    for i, x in enumerate(cmd.get_names()):
        c = TABLEAU_10_COLORS[i % tab10_len]
        sele_str = x if not sele else f"x and {sele}"
        cmd.color(c, sele_str)


def tmalign_all_to_(sele: str = ""):
    sele_str = "sele" if not sele else sele
    for x in cmd.get_names():
        cmd.do(f"tmalign {x}, {sele_str}")


cmd.extend("recolor_tab10", recolor_tab10)
cmd.extend("tmalign_all_to", tmalign_all_to_)


cmd.set("ambient_occlusion_mode", 1)
cmd.set("ray_shadow", 0)
cmd.set("spec_reflect", 0)
cmd.set("cartoon_discrete_colors", 1)
cmd.set("cartoon_loop_radius", 0.4)
cmd.set("cartoon_smooth_loops", 0)
cmd.set("cartoon_oval_length", 1.1)
cmd.set("cartoon_oval_width", 0.4)
cmd.set("valence", 0)
