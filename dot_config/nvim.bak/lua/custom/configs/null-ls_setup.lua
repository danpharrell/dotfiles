local present, null_ls = pcall(require, "null-ls")

if not present then
	return
end

local b = null_ls.builtins

null_ls.setup({
	sources = {
		b.formatting.deno_fmt,
		b.formatting.prettier.with({ filetypes = { "html", "markdown", "css", "json", "yaml" } }),

		b.formatting.stylua,

		-- python
		b.formatting.black.with({ extra_args = { "--line-length", "120" } }),
		-- b.diagnostics.mypy,

		-- Shell
		b.formatting.shfmt,
		b.diagnostics.shellcheck.with({ diagnostics_format = "#{m} [#{c}]" }),

		-- cpp
		b.formatting.clang_format,
	},
	debug = true,
})
