local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities
local lspconfig = require("lspconfig")

local function get_python_path()
	-- Use activated conda env.
	if vim.env.CONDA_PYTHON_EXE then
		return vim.env.CONDA_PYTHON_EXE
	elseif vim.env.CONDA_PREFIX then
		return vim.env.CONDA_PREFIX .. "/bin/python"
	end

	-- Fallback to system Python.
	return vim.fn.exepath("python3") or vim.fn.exepath("python") or "python"
end

function table.my_contains(table, element)
	for _, value in pairs(table) do
		if value == element then
			return true
		end
	end
	return false
end

local function combine_two_tables(t1, t2)
	local ret = {}
	local n = 0
	for _, v in ipairs(t1) do
		n = n + 1
		ret[n] = v
	end
	for _, v in ipairs(t2) do
		n = n + 1
		ret[n] = v
	end
	return ret
end

local function switch_source_header_splitcmd(bufnr, splitcmd)
	bufnr = require("lspconfig").util.validate_bufnr(bufnr)
	local clangd_client = require("lspconfig").util.get_active_client_by_name(bufnr, "clangd")
	local params = { uri = vim.uri_from_bufnr(bufnr) }
	if clangd_client then
		clangd_client.request("textDocument/switchSourceHeader", params, function(err, result)
			if err then
				error(tostring(err))
			end
			if not result then
				print("Corresponding file can’t be determined")
				return
			end
			vim.api.nvim_command(splitcmd .. " " .. vim.uri_to_fname(result))
		end, bufnr)
	else
		print("textDocument/switchSourceHeader is not supported by the clangd server active on the current buffer")
	end
end

local npm_servers = { "html", "jsonls", "pyright", "bashls", "emmet_ls" }
local non_npm_servers = { "rust_analyzer", "cssls", "html", "tsserver" }

local all_servers = combine_two_tables(npm_servers, non_npm_servers)

local mod_attach = function(client, bufnr)
	if table.my_contains(npm_servers, client.name) then
		-- local mason_registry = require "mason-registry"

		local runprecmd = { "node" }
		-- local server_exe = client.config.cmd[1]
		-- local server_cmd_path = mason_registry.get_package(client.name)
		for _, v in ipairs(client.config.cmd) do
			table.insert(runprecmd, v)
			-- if v == server_exe then
			-- 	table.insert(runprecmd, server_cmd_path:get_install_path())
			-- else
			-- 	table.insert(runprecmd, v)
			-- end
		end
		client.config.cmd = runprecmd
		-- if client.name == "pyright" then
		--	 client.config.settings.python.pythonPath = get_python_path()
		-- end
		if client.config.settings.python then
			client.config.settings.python.pythonPath = get_python_path()
		end
	end
	on_attach(client, bufnr)
end

for _, lsp in ipairs(all_servers) do
	lspconfig[lsp].setup({
		on_attach = mod_attach,
		capabilities = capabilities,
	})
end

lspconfig.clangd.setup({
	on_attach = on_attach,
	capabilities = capabilities,
	cmd = { "clangd", "-j", "8" },
	commands = {
		ClangdSwitchSourceHeader = {
			function()
				switch_source_header_splitcmd(0, "edit")
			end,
			description = "Open source/header in current buffer",
		},
		ClangdSwitchSourceHeaderVSplit = {
			function()
				switch_source_header_splitcmd(0, "vsplit")
			end,
			description = "Open source/header in a new vsplit",
		},
		ClangdSwitchSourceHeaderSplit = {
			function()
				switch_source_header_splitcmd(0, "split")
			end,
			description = "Open source/header in a new split",
		},
	},
})

-- lua_ls specialize
lspconfig.lua_ls.setup({
	on_attach = on_attach,
	capabilities = capabilities,
	settings = {
		runtime = {
			-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
			version = "LuaJIT",
		},
		Lua = {
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				checkThirdParty = false,
				library = vim.api.nvim_get_runtime_file("", true),
				maxPreload = 100000,
				preloadFileSize = 10000,
			},
		},
	},
})

-- ruff-lsp specialize
lspconfig.ruff_lsp.setup({
	on_attach = function(client, bufnr)
		-- Enable completion triggered by <c-x><c-o>
		vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

		-- Mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local bufopts = { noremap = true, silent = true, buffer = bufnr }
		vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)

		vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
		vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
		vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
		vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
		vim.keymap.set("n", "<Leader>wa", vim.lsp.buf.add_workspace_folder, bufopts)
		vim.keymap.set("n", "<Leader>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
		vim.keymap.set("n", "<Leader>wl", function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, bufopts)
		vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, bufopts)
		vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, bufopts)
		vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, bufopts)
		vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
		vim.keymap.set("n", "<space>f", function()
			vim.lsp.buf.format({ async = true })
		end, bufopts)
		on_attach(client, bufnr)
	end,
	init_options = {
		settings = {
			-- Any extra CLI arguments for `ruff` go here.
			args = {},
		},
	},
})
