local override = require("custom.override")

local M = {
	{
		"williamboman/mason.nvim",
		opts = {
			ensure_installed = {
				-- lua stuff
				"lua-language-server",
				"stylua",

				"clang-format",
				"clangd",

				"pyright",
				"mypy",
				"ruff-lsp",

				"rust-analyzer",
				"codelldb",

				-- web dev
				"css-lsp",
				"html-lsp",
				"typescript-language-server",

				"bash-language-server",
				"emmet-ls",
				"json-lsp",

				"prettier",

				-- shell
				"shellcheck",
			},
		},
	},
	{
		"Olical/conjure",
		lazy = false,
		dependencies = { "nvim-treesitter/nvim-treesitter" },
	},
	{
		"statox/vim-compare-lines",
		lazy = false,
	},
	{
		'echasnovski/mini.align',
		version = '*',
		lazy = false,
	},
	{ "nvim-tree/nvim-tree.lua", opts = override.nvimtree },
	{ "nvim-treesitter/nvim-treesitter", opts = override.treesitter },
	{
		"lukas-reineke/indent-blankline.nvim",
		opts = override.indentblankline,
	},
	{
		"rcarriga/nvim-dap-ui",
		dependencies = {
			"mfussenegger/nvim-dap",
			config = function()
				require("nvim-lspconfig")
				require("nvim-dap").setup()
			end,
		},
		config = function()
			require("nvim-dap")
			require("dapui").setup({
				icons = { expanded = "", collapsed = "", current_frame = "" },
				mappings = {
					-- Use a table to apply multiple mappings
					expand = { "<CR>", "<2-LeftMouse>" },
					open = "o",
					remove = "d",
					edit = "e",
					repl = "r",
					toggle = "t",
				},
				-- Use this to override mappings for specific elements
				element_mappings = {
					-- Example:
					-- stacks = {
					--   open = "<CR>",
					--   expand = "o",
					-- }
				},
				-- Expand lines larger than the window
				-- Requires >= 0.7
				expand_lines = vim.fn.has("nvim-0.7") == 1,
				-- Layouts define sections of the screen to place windows.
				-- The position can be "left", "right", "top" or "bottom".
				-- The size specifies the height/width depending on position. It can be an Int
				-- or a Float. Integer specifies height/width directly (i.e. 20 lines/columns) while
				-- Float value specifies percentage (i.e. 0.3 - 30% of available lines/columns)
				-- Elements are the elements shown in the layout (in order).
				-- Layouts are opened in order so that earlier layouts take priority in window sizing.
				layouts = {
					{
						elements = {
							-- Elements can be strings or table with id and size keys.
							{ id = "scopes", size = 0.25 },
							"breakpoints",
							"stacks",
							"watches",
						},
						size = 40, -- 40 columns
						position = "left",
					},
					{
						elements = {
							"repl",
							"console",
						},
						size = 0.25, -- 25% of total lines
						position = "bottom",
					},
				},
				controls = {
					-- Requires Neovim nightly (or 0.8 when released)
					enabled = true,
					-- Display controls in this element
					element = "repl",
					icons = {
						pause = "",
						play = "",
						step_into = "",
						step_over = "",
						step_out = "",
						step_back = "",
						run_last = "",
						terminate = "",
					},
				},
				floating = {
					max_height = nil, -- These can be integers or a float between 0 and 1.
					max_width = nil, -- Floats will be treated as percentage of your screen.
					border = "single", -- Border style. Can be "single", "double" or "rounded"
					mappings = {
						close = { "q", "<Esc>" },
					},
				},
				windows = { indent = 1 },
				render = {
					max_type_length = nil, -- Can be integer or nil.
					max_value_lines = 100, -- Can be integer or nil.
				},
			})
		end,
	},
	{
		"williamboman/mason-lspconfig.nvim",
		dependencies = { "williamboman/mason.nvim" },
		config = function()
			require("plugins.configs.lspconfig")
			require("custom.configs.lspconfig_setup")
		end,
	},
	{
		"simrat39/rust-tools.nvim",
		dependencies = { "neovim/nvim-lspconfig" },
		config = function()
			local rt = require("rust-tools")
			rt.setup({
				server = {
					on_attach = function(_, bufnr)
						-- Hover actions
						vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
						-- Code action groups
						vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
					end,
					settings = {
						["rust-analyzer"] = {
							check = {
								command = "clippy",
								extraArgs = { "--all", "--", "-W", "clippy::all" },
							},
						},
					},
				},
			})
		end,
	},
	{
		"folke/which-key.nvim",
		keys = { "<leader>", '"', "'", "`"},
		enabled = true,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{ "williamboman/mason-lspconfig.nvim" },
			{
				"jose-elias-alvarez/null-ls.nvim",
				config = function()
					require("custom.configs.null-ls_setup")
				end,
			},
		},
	},
	{
		"ethanholz/nvim-lastplace",
		config = function()
			require("nvim-lastplace").setup({
				lastplace_ignore_buftype = { "quickfix", "nofile", "help" },
				lastplace_ignore_filetype = { "gitcommit", "gitrebase", "svn", "hgcommit" },
				lastplace_open_folds = true,
			})
		end,
	},
}

return M
