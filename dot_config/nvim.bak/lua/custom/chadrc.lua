-- Just an example, supposed to be placed in /lua/custom/

local M = {}

M.ui = {
	theme = "one_light",
	-- theme = "danone",
	clipboard = "unnamedplus",
}

-- Doesn't work
-- M.options = {
-- 	disable_default_plugins = {
-- 		"2html_plugin",
-- 		"getscript",
-- 		"getscriptPlugin",
-- 		--"gzip",
-- 		"logipat",
-- 		-- "netrw",
-- 		-- "netrwPlugin",
-- 		"netrwSettings",
-- 		"netrwFileHandlers",
-- 		"matchit",
-- 		--"tar",
-- 		--"tarPlugin",
-- 		"rrhelper",
-- 		"spellfile_plugin",
-- 		--"vimball",
-- 		--"vimballPlugin",
-- 		"zip",
-- 		"zipPlugin",
-- 	},
-- }

M.plugins = "custom.plugins"

M.mappings = require("custom.mappings")

-- vim.g.maplocalleader = "\\"

vim.opt.list = true
vim.opt.cursorline = false
local tmp_listchars = vim.opt.listchars:get()
-- tmp_listchars['tab'] = '→ '
tmp_listchars["tab"] = "| "
tmp_listchars["lead"] = "·"
tmp_listchars["eol"] = "↴"
vim.opt.listchars = tmp_listchars

vim.filetype.add({
	pattern = {
		["(.+).tmpl"] = function(path, bufnr, ext)
			return vim.fn.fnamemodify(ext, ":e")
		end,
	},
})

vim.api.nvim_create_autocmd("FileType", {
	pattern = "lua",
	-- pattern = {"*.lua", "*.lua.tmpl"},
	callback = function(_)
		vim.opt_local.expandtab = false
		vim.opt_local.tabstop = 4
		vim.opt_local.shiftwidth = 4
		vim.opt_local.colorcolumn = "120"
	end,
})

vim.diagnostic.config({
	virtual_text = false, -- Turn off inline diagnostics
})


local opts = { noremap=true, silent=true }
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
-- Show all diagnostics on current line in floating window
vim.api.nvim_set_keymap("n", "<Leader>ld", ":lua vim.diagnostic.open_float()<CR>", opts)
-- Go to next diagnostic (if there are multiple on the same line, only shows
-- one at a time in the floating window)
vim.api.nvim_set_keymap("n", "<Leader>ln", ":lua vim.diagnostic.goto_next()<CR>", opts)
-- Go to prev diagnostic (if there are multiple on the same line, only shows
-- one at a time in the floating window)
vim.api.nvim_set_keymap("n", "<Leader>lN", ":lua vim.diagnostic.goto_prev()<CR>", opts)

vim.api.nvim_set_keymap("n", "<Leader>lq", ":lua vim.diagnostic.setloclist()<CR>", opts)

vim.api.nvim_set_keymap("n", "<leader>lD", ":lua vim.lsp.buf.type_definition()<CR>", opts)

return M
