-- oil config value lsp_rename_autosave has moved to lsp_file_methods.autosave_changes.

-- Globals {{{
-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "
-- }}}

-- Plugin Manager Setup {{{
-- https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
-- }}}

-- Configure Plugins {{{

-- Plugin Helpers and Static Configs {{{

local linters = {
	css = { "stylelint" },
	sh = { "shellcheck" },
	markdown = { "markdownlint" },
	yaml = { "yamllint" },
	python = { "ruff" },
	terraform = { "tflint" },
	-- toml = {},
}

local conform_formatters = {
	-- lua = { "stylua" },
	terraform = { "terraform_fmt" },
	python = { "ruff_fix", "ruff_format" },
	rust = { "rustfmt" },
	cpp = { "clang-format" },
	cmake = { "cmake_format" },
	yaml = { "yamlfmt" },
	json = { "prettier" },
	markdown = { "injected", "mdformat" },
	sh = { "shellcheck", "shfmt" },
	-- Use the "*" filetype to run formatters on all filetypes.
	["*"] = { "codespell" },
	-- Use the "_" filetype to run formatters on filetypes that don't
	-- have other formatters configured.
	["_"] = { "trim_whitespace" },
}

local dependencies = {
	"debugpy", -- nvim-dap-python
}

local lsps_to_setup = {
	lua_ls = {
		settings = {
			Lua = {
				diagnostics = { globals = { "vim" } },
				workspace = {
					library = vim.api.nvim_get_runtime_file("", true),
					checkThirdParty = false,
				},
				telemetry = { enable = false },
			},
		},
	},
	terraformls = {},
	pyright = {},
	jsonls = {},
	bashls = {},
	yamlls = {},
	autotools_ls = {},
	ruff_lsp = {},
	taplo = {},
	typos_lsp = {
		init_options = {
			config = ".typos.toml",
		},
	},
	cmake = {},
}

local function get_python_path()
	-- Use activated conda env.
	if vim.env.CONDA_PYTHON_EXE then
		return vim.env.CONDA_PYTHON_EXE
	elseif vim.env.CONDA_PREFIX then
		return vim.env.CONDA_PREFIX .. "/bin/python"
	end

	-- Fallback to system Python.
	return vim.fn.exepath("python3") or vim.fn.exepath("python") or "python"
end

function table.my_contains(table, element)
	for _, value in pairs(table) do
		if value == element then
			return true
		end
	end
	return false
end

local nmap = function(keys, func, desc, bufnr)
	if desc then
		desc = "LSP: " .. desc
	end
	vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
end

local on_attach = function(client, bufnr)
	if table.my_contains({ "html", "jsonls", "pyright", "bashls" }, client.name) then
		if client.config.settings.python then
			client.config.settings.python.pythonPath = get_python_path()
		end
	end
	-- NOTE: Remember that lua is a real programming language, and as such it is possible
	-- to define small helper and utility functions so you don't have to repeat yourself
	-- many times.
	--
	nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame", bufnr)
	nmap("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction", bufnr)

	nmap("gd", require("telescope.builtin").lsp_definitions, "[G]oto [D]efinition", bufnr)
	nmap("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences", bufnr)
	nmap("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation", bufnr)
	nmap("<leader>D", require("telescope.builtin").lsp_type_definitions, "Type [D]efinition", bufnr)
	nmap("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]symbols", bufnr)
	nmap("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]symbols", bufnr)

	-- See `:help K` for why this keymap
	nmap("K", vim.lsp.buf.hover, "Hover Documentation", bufnr)
	nmap("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation", bufnr)

	-- Lesser used LSP functionality
	nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration", bufnr)
	nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder", bufnr)
	nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder", bufnr)
	nmap("<leader>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, "[W]orkspace [L]ist Folders", bufnr)

	-- Create a command `:Format` local to the LSP buffer
	vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
		vim.lsp.buf.format()
	end, { desc = "Format current buffer with LSP" })
end

local dontInstall = {
	-- installed externally due to its plugins: https://github.com/williamboman/mason.nvim/issues/695
	"stylelint",
	-- not real formatters, but pseudo-formatters from conform.nvim
	"trim_whitespace",
	"trim_newlines",
	"squeeze_blanks",
	"injected",
}

local function conform_to_mason(my_conform_tools)
	local conform_to_mason_map = {
		clang_format = "clang-format",
		cmake_format = "cmakelang",
		ruff_fix = "ruff-lsp",
		ruff_format = "ruff-lsp",
	}
	local ignore = { trim_whitespace = true, rustfmt = true, injected = true, terraform_fmt = true }
	local ret = {}
	local my_conform_tools_l = vim.tbl_flatten(vim.tbl_values(my_conform_tools))
	for _, c_tool in ipairs(my_conform_tools_l) do
		if ignore[c_tool] == nil then
			local maybe_conf = conform_to_mason_map[c_tool]
			if maybe_conf then
				table.insert(ret, maybe_conf)
			else
				table.insert(ret, c_tool)
			end
		end
	end
	return ret
end

-- }}}

require("lazy").setup({

	-- Theming, Colors, Accessibility {{{
	{
		"projekt0n/github-nvim-theme",
		lazy = false, -- make sure we load this during startup if it is your main colorscheme
		priority = 1000, -- make sure to load this before all the other start plugins
		config = function()
			require("github-theme").setup({})
			vim.cmd("colorscheme github_light")
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		-- See `:help lualine.txt`
		opts = {
			options = {
				icons_enabled = false,
				theme = "github_light",
				component_separators = "|",
				section_separators = "",
			},
		},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
		},
		-- Pin because projekt0n/github-nvim-theme is not updated
		tag = "v0.9.2",
		build = ":TSUpdate",
		config = function()
			require("nvim-treesitter.configs").setup({
				-- Add languages to be installed here that you want installed for treesitter
				ensure_installed = {
					"c",
					"lua",
					"vim",
					"vimdoc",
					"query",
					-- end required languages
					"cpp",
					"go",
					"python",
					"rust",
					"bash",
					"cmake",
					"terraform",
					"hcl",
					"toml",
					"markdown",
					"markdown_inline",
				},

				-- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
				auto_install = false,

				highlight = { enable = true },
				indent = { enable = false }, -- https://github.com/nvim-treesitter/nvim-treesitter/issues/5914
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "<c-space>",
						node_incremental = "<c-space>",
						scope_incremental = "<c-s>",
						node_decremental = "<M-space>",
					},
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
						keymaps = {
							-- You can use the capture groups defined in textobjects.scm
							["aa"] = "@parameter.outer",
							["ia"] = "@parameter.inner",
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
						},
					},
					move = {
						enable = true,
						set_jumps = true, -- whether to set jumps in the jumplist
						goto_next_start = {
							["]m"] = "@function.outer",
							["]]"] = "@class.outer",
						},
						goto_next_end = {
							["]M"] = "@function.outer",
							["]["] = "@class.outer",
						},
						goto_previous_start = {
							["[m"] = "@function.outer",
							["[["] = "@class.outer",
						},
						goto_previous_end = {
							["[M"] = "@function.outer",
							["[]"] = "@class.outer",
						},
					},
					swap = {
						enable = true,
						swap_next = {
							["<leader>a"] = "@parameter.inner",
						},
						swap_previous = {
							["<leader>A"] = "@parameter.inner",
						},
					},
				},
			})
		end,
	},
	{
		-- Add indentation guides even on blank lines
		"lukas-reineke/indent-blankline.nvim",
		-- Enable `lukas-reineke/indent-blankline.nvim`
		-- See `:help ibl`
		main = "ibl",
		opts = {},
	},
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		config = function(opts)
			local wk = require("which-key")
			wk.setup(opts)
			wk.register({
				["<leader>c"] = { name = "[C]ode", _ = "which_key_ignore" },
				["<leader>d"] = { name = "[D]ocument", _ = "which_key_ignore" },
				["<leader>g"] = { name = "[G]it", _ = "which_key_ignore" },
				["<leader>h"] = { name = "Git [H]unk", _ = "which_key_ignore" },
				["<leader>r"] = { name = "[R]ename", _ = "which_key_ignore" },
				["<leader>s"] = { name = "[S]earch", _ = "which_key_ignore" },
				["<leader>t"] = { name = "[T]oggle", _ = "which_key_ignore" },
				["<leader>w"] = { name = "[W]orkspace", _ = "which_key_ignore" },
			})
			wk.register({
				["<leader>"] = { name = "VISUAL <leader>" },
				["<leader>h"] = { "Git [H]unk" },
			}, { mode = "v" })
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			-- Snippet Engine & its associated nvim-cmp source
			"L3MON4D3/LuaSnip",
			"saadparwaiz1/cmp_luasnip",

			-- Adds LSP completion capabilities
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-cmdline",

			-- Adds a number of user-friendly snippets
			"rafamadriz/friendly-snippets",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			require("luasnip.loaders.from_vscode").lazy_load()
			luasnip.config.setup({})

			cmp.setup({
				snippet = {
					expand = function(args)
						luasnip.lsp_expand(args.body)
					end,
				},
				completion = {
					completeopt = "menu,menuone,noinsert",
				},
				mapping = cmp.mapping.preset.insert({
					["<C-n>"] = cmp.mapping.select_next_item(),
					["<C-p>"] = cmp.mapping.select_prev_item(),
					["<C-b>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete({}),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Replace,
						select = true,
					}),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif luasnip.expand_or_locally_jumpable() then
							luasnip.expand_or_jump()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<S-Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif luasnip.locally_jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end, { "i", "s" }),
				}),
				sources = {
					{ name = "nvim_lsp" },
					{ name = "luasnip" },
					{ name = "path" },
				},
			})
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		-- follow latest release.
		version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
		-- install jsregexp (optional!).
		build = "make install_jsregexp",
		config = function()
			local ls = require("luasnip")
			vim.keymap.set({ "i" }, "<C-K>", function()
				ls.expand()
			end, { silent = true })
			vim.keymap.set({ "i", "s" }, "<C-L>", function()
				ls.jump(1)
			end, { silent = true })
			vim.keymap.set({ "i", "s" }, "<C-J>", function()
				ls.jump(-1)
			end, { silent = true })

			vim.keymap.set({ "i", "s" }, "<C-E>", function()
				if ls.choice_active() then
					ls.change_choice(1)
				end
			end, { silent = true })
			-- Snippets {{{

			-- Python function definition {{{
			local s = ls.snippet
			local i = ls.insert_node
			local t = ls.text_node
			local c = ls.choice_node
			local sn = ls.snippet_node
			local isn = ls.indent_snippet_node
			local fmt = require("luasnip.extras.fmt").fmt
			local types = require("luasnip.util.types")

			local function node_with_virtual_text(pos, node, text)
				local nodes
				if node.type == types.textNode then
					node.pos = 2
					nodes = { i(1), node }
				else
					node.pos = 1
					nodes = { node }
				end
				return sn(pos, nodes, {
					node_ext_opts = {
						active = {
							-- override highlight here ("GruvboxOrange").
							virt_text = { { text, "GruvboxOrange" } },
						},
					},
				})
			end

			local function nodes_with_virtual_text(nodes, opts)
				if opts == nil then
					opts = {}
				end
				local new_nodes = {}
				for pos, node in ipairs(nodes) do
					if opts.texts[pos] ~= nil then
						node = node_with_virtual_text(pos, node, opts.texts[pos])
					end
					table.insert(new_nodes, node)
				end
				return new_nodes
			end

			local function choice_text_node(pos, choices, opts)
				choices = nodes_with_virtual_text(choices, opts)
				return c(pos, choices, opts)
			end

			local ct = choice_text_node

			ls.add_snippets("python", {
				s(
					"d",
					fmt(
						[[
		def {func}({args}){ret}:
			{doc}{body}
	]],
						{
							func = i(1),
							args = i(2),
							ret = c(3, {
								t(""),
								sn(nil, {
									t(" -> "),
									i(1),
								}),
							}),
							doc = isn(4, {
								ct(1, {
									t(""),
									-- NOTE we need to surround the `fmt` with `sn` to make this work
									sn(
										1,
										fmt(
											[[
			"""{desc}"""

			]],
											{ desc = i(1) }
										)
									),
									sn(
										2,
										fmt(
											[[
			"""{desc}

			Args:
			{args}

			Returns:
			{returns}
			"""

			]],
											{
												desc = i(1),
												args = i(2), -- TODO should read from the args in the function
												returns = i(3),
											}
										)
									),
								}, {
									texts = {
										"(no docstring)",
										"(single line docstring)",
										"(full docstring)",
									},
								}),
							}, "$PARENT_INDENT\t"),
							body = i(0),
						}
					)
				),
			})
			-- }}}

			-- Python mainstart {{{
			ls.add_snippets("python", {
				s(
					"mainstart",
					fmt(
						[[#!/usr/bin/env python
"""
{filedescription}
"""

import argparse
import logging
import sys
import os

logging.basicConfig(level=os.getenv("LOGLEVEL", "INFO").upper(), stream=sys.stdout)
log = logging.getLogger(__name__)


def parse_args(cmdline_args: list[str]) -> argparse.Namespace:
	parser = argparse.ArgumentParser(description="{filedescription}")
	return parser.parse_args(cmdline_args)


def main(cmdline_args: list[str]) -> None:
	parsed_args = parse_args(cmdline_args)
	pass


if __name__ == "__main__":
	main(sys.argv[1:])]],
						{
							filedescription = i(1),
						},
						{ repeat_duplicates = true }
					)
				),
			})
			-- }}}

			-- Python arg add {{{
			ls.add_snippets("python", {
				s("parser", {
					t("parser.add_argument(\"--"),
					i(1, "arg"),
					t("\", "),
					t("type="),
					c(2, { t("str"), t("int"), t("float"), t("bool"), i(1, "arg") }),
					t(", "),
					t("help=\""),
					i(3, "Description"),
					t("\", "),
					t("default="),
					i(4, "None"),
					t(", required="),
					c(5, { t("True"), t("False") }),
					c(6, {
						t(", nargs=\""),
						{ t(", nargs=\""), i(1, "?nargs"), t("\"") },
						t(""),
					}),
					t("\""),
					c(7, {
						{ t(", choices=["), i(1, "choices"), t("])") },
						t(""),
					}),
					t(")"),
				}),
			})
			-- }}}

			-- Python Pathlib mkdir {{{
			ls.add_snippets("python", {
				s("mkd", {
					t("mkdir(exist_ok=True, parents=True)"),
				}),
			})
			-- }}}
		end,
	},
	{
		"kevinhwang91/nvim-ufo",
		event = { "BufRead" },
		dependencies = "kevinhwang91/promise-async",
		config = function()
			vim.o.foldcolumn = "1" -- '0' is not bad
			vim.o.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
			vim.o.foldlevelstart = 99
			vim.o.foldenable = true

			-- Using ufo provider need remap `zR` and `zM`. If Neovim is 0.6.1, remap yourself
			vim.keymap.set("n", "zR", require("ufo").openAllFolds)
			vim.keymap.set("n", "zM", require("ufo").closeAllFolds)
			vim.keymap.set("n", "zK", function()
				local winid = require("ufo").peekFoldedLinesUnderCursor()
				if not winid then
					vim.lsp.buf.hover()
				end
			end, { desc = "Peek Fold" })

			local ftMap = {
				git = "",
			}
			local foldingrange = require("ufo.model.foldingrange")
			local bufmanager = require("ufo.bufmanager")

			local CustomMarkerProvider = {}

			function CustomMarkerProvider.getFolds(bufnr)
				local buf = bufmanager:get(bufnr)
				if not buf then
					return
				end
				local fmrOpen = vim.opt.foldmarker:get()[1]
				local fmrClose = vim.opt.foldmarker:get()[2]
				local commentstring = vim.opt.commentstring:get()
				if commentstring == "/*%s*/" then
					-- Hack for c++ and other // and /* */ langs
					commentstring = "//%s"
				end
				local openRegx = commentstring .. "*.-" .. fmrOpen
				local closeRegx = commentstring .. "*.-" .. fmrClose
				local summaryRegx = openRegx .. "%s*(.*)"
				local lines = buf:lines(1, -1)

				local ranges = {}
				local stack = {}

				for lnum, line in ipairs(lines) do
					-- Check for start marker
					if line:match(openRegx) then
						table.insert(stack, lnum)
						-- Check for end marker
					elseif line:match(closeRegx) then
						local startLnum = table.remove(stack)
						if startLnum then
							local summary = lines[startLnum]:match(summaryRegx)
							table.insert(ranges, foldingrange.new(startLnum - 1, lnum - 1, summary))
						end
					end
				end

				return ranges
			end

			local function customizeSelector(bufnr)
				local ranges = CustomMarkerProvider.getFolds(bufnr)
				local maybe_additional_ranges = require("ufo").getFolds(bufnr, "treesitter") or {}
				if next(maybe_additional_ranges) ~= nil then
					ranges = vim.list_extend(ranges, maybe_additional_ranges)
				else
					ranges = vim.list_extend(ranges, require("ufo").getFolds(bufnr, "indent")) or {}
				end
				return ranges
			end

			require("ufo").setup({
				provider_selector = function(bufnr, filetype, buftype)
					return ftMap[filetype] or customizeSelector
				end,
			})
		end,
	},

	-- }}}
	-- }}}

	-- Formatting {{{
	{
		"stevearc/conform.nvim",
		opts = {
			formatters_by_ft = conform_formatters,
		},
		cmd = { "ConformInfo" },
		event = { "BufWritePre" },
		keys = {
			{
				-- Customize or remove this keymap to your liking
				"<leader>fm",
				function()
					require("conform").format({ async = true, lsp_fallback = true })
				end,
				mode = "",
				desc = "Format buffer",
			},
		},
	},
	-- }}}
	-- Linting {{{
	{
		"mfussenegger/nvim-lint",
		lazy = true,
		event = { "BufWritePost", "BufReadPost", "InsertLeave" },
		config = function()
			local lint = require("lint")

			lint.linters_by_ft = linters

			local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })

			vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
				group = lint_augroup,
				callback = function()
					lint.try_lint()
				end,
			})

			vim.keymap.set("n", "<leader>l", function()
				lint.try_lint()
			end, { desc = "Trigger linting for current file" })
		end,
	},
	-- }}}

	-- Language Server {{{
	{
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		event = "VeryLazy",
		dependencies = {
			{ "williamboman/mason.nvim",           config = true },
			{ "williamboman/mason-lspconfig.nvim", config = true },
		},
		config = function()
			-- get all lsps, formatters, & extra tools and merge them into one list
			local tools = conform_to_mason(conform_formatters)
			tools = vim.list_extend(tools, vim.tbl_flatten(vim.tbl_values(linters)))
			local lspconfig_to_mason_package = require("mason-lspconfig").get_mappings()["lspconfig_to_mason"]

			-- tools = vim.list_extend(tools, vim.tbl_flatten(vim.tbl_values(lspconfig_to_mason_package)))
			for k, _ in pairs(lsps_to_setup) do
				local converted_name = lspconfig_to_mason_package[k]
				if converted_name ~= nil then
					table.insert(tools, converted_name)
				end
			end
			tools = vim.list_extend(tools, dependencies)

			-- only unique tools
			table.sort(tools)
			tools = vim.fn.uniq(tools)

			require("mason-tool-installer").setup({
				ensure_installed = tools,
				run_on_start = false, -- triggered manually, since not working with lazy-loading
			})

			-- clean unused & install missing
			vim.defer_fn(vim.cmd.MasonToolsInstall, 500)
			vim.defer_fn(vim.cmd.MasonToolsClean, 1000) -- delayed, so noice.nvim is loaded before
		end,
	},
	{
		-- LSP Configuration & Plugins
		"neovim/nvim-lspconfig",
		dependencies = {
			-- Automatically install LSPs to stdpath for neovim
			{ "williamboman/mason.nvim", config = true },
			"williamboman/mason-lspconfig.nvim",
			"hrsh7th/nvim-cmp",
			-- "mfussenegger/nvim-lint", -- maps to "lint" in require

			-- Useful status updates for LSP
			-- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
			-- This is the status updates in the bottom right corner
			{
				"j-hui/fidget.nvim",
				opts = {},
			},

			-- Additional lua configuration, makes nvim stuff amazing!
			"folke/neodev.nvim",
		},
		config = function()
			require("mason").setup()
			local registry = require("mason-registry")
			registry.refresh(function()
				registry.get_package("lua-language-server")
			end)
			require("mason-lspconfig").setup()

			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

			local lspconfig = require("lspconfig")
			for k, v in pairs(lsps_to_setup) do
				v["capabilities"] = capabilities
				v["on_attach"] = on_attach
				lspconfig[k].setup(v)
			end
		end,
	},
	{
		"folke/neodev.nvim",
		config = function()
			require("neodev").setup({
				library = { plugins = { "nvim-dap-ui" }, types = true },
			})
		end,
	},
	{ "williamboman/mason-lspconfig.nvim" },
	{
		'mrcjkb/rustaceanvim',
		version = '^4', -- Recommended
		ft = { 'rust' },
		config = function()
			vim.g.rustaceanvim = {}
		end
	},
	-- }}}

	-- Debugging  {{{
	{
		"mfussenegger/nvim-dap",
		keys = {
			{
				"<leader>dc",
				function()
					require("dap").continue()
				end,
				desc = "Start/Continue Debugger",
			},
			{
				"<leader>db",
				function()
					require("dap").toggle_breakpoint()
				end,
				desc = "Add Breakpoint",
			},
			{
				"<leader>dt",
				function()
					require("dap").terminate()
				end,
				desc = "Terminate Debugger",
			},
		},
	},

	-- UI for the debugger
	-- - the debugger UI is also automatically opened when starting/stopping the debugger
	-- - toggle debugger UI manually with `<leader>du`
	{
		"rcarriga/nvim-dap-ui",
		dependencies = "mfussenegger/nvim-dap",
		keys = {
			{
				"<leader>du",
				function()
					require("dapui").toggle()
				end,
				desc = "Toggle Debugger UI",
			},
		},
		-- automatically open/close the DAP UI when starting/stopping the debugger
		config = function(_, opts)
			local listener = require("dap").listeners
			listener.after.event_initialized["dapui_config"] = function()
				require("dapui").open()
			end
			listener.before.event_terminated["dapui_config"] = function()
				require("dapui").close()
			end
			listener.before.event_exited["dapui_config"] = function()
				require("dapui").close()
			end
			require("dapui").setup(opts)
		end,
	},

	-- Configuration for the python debugger
	-- - configures debugpy for us
	-- - uses the debugpy installation from mason
	{
		"mfussenegger/nvim-dap-python",
		dependencies = "mfussenegger/nvim-dap",
		config = function()
			-- uses the debugypy installation by mason
			local debugpyPythonPath = require("mason-registry").get_package("debugpy"):get_install_path()
				.. "/venv/bin/python3"
			require("dap-python").setup(debugpyPythonPath, {})
		end,
	},

	-- }}}

	-- Tools & QOL Plugins {{{
	{ "numToStr/Comment.nvim",            opts = {} },
	{
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			-- Fuzzy Finder Algorithm which requires local dependencies to be built.
			-- Only load if `make` is available. Make sure you have the system
			-- requirements installed.
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				-- NOTE: If you are having trouble with this installation,
				--       refer to the README for telescope-fzf-native for more instructions.
				build = "make",
				cond = function()
					return vim.fn.executable("make") == 1
				end,
			},
		},
		config = function()
			require("telescope").setup({
				defaults = {
					mappings = {
						i = {
							["<C-u>"] = false,
							["<C-d>"] = false,
						},
					},
					wrap_results = true,
					-- sorting_strategy = "ascending",
				},
				pickers = {
					oldfiles = {
						initial_mode = "normal",
					},
					colorscheme = {
						enable_preview = true,
					},
					find_files = {
						hidden = true,
					},
					buffers = {
						mappings = {
							i = {
								["<C-d>"] = require("telescope.actions").delete_buffer,
							},
						},
					},
				},
			})
			-- Enable telescope fzf native, if installed
			pcall(require("telescope").load_extension, "fzf")

			-- Telescope live_grep in git root
			-- Function to find the git root directory based on the current buffer's path
			local function find_git_root()
				-- Use the current buffer's path as the starting point for the git search
				local current_file = vim.api.nvim_buf_get_name(0)
				local current_dir
				local cwd = vim.fn.getcwd()
				-- If the buffer is not associated with a file, return nil
				if current_file == "" then
					current_dir = cwd
				else
					-- Extract the directory from the current file's path
					current_dir = vim.fn.fnamemodify(current_file, ":h")
				end

				-- Find the Git root directory from the current file's path
				local git_root =
					vim.fn.systemlist("git -C " .. vim.fn.escape(current_dir, " ") .. " rev-parse --show-toplevel")[1]
				if vim.v.shell_error ~= 0 then
					print("Not a git repository. Searching on current working directory")
					return cwd
				end
				return git_root
			end

			-- Custom live_grep function to search in git root
			local function live_grep_git_root()
				local git_root = find_git_root()
				if git_root then
					require("telescope.builtin").live_grep({
						search_dirs = { git_root },
					})
				end
			end

			vim.api.nvim_create_user_command("LiveGrepGitRoot", live_grep_git_root, {})

			-- See `:help telescope.builtin`
			vim.keymap.set(
				"n",
				"<leader>?",
				require("telescope.builtin").oldfiles,
				{ desc = "[?] Find recently opened files" }
			)
			vim.keymap.set(
				"n",
				"<leader><space>",
				require("telescope.builtin").buffers,
				{ desc = "[ ] Find existing buffers" }
			)
			vim.keymap.set("n", "<leader>/", function()
				-- You can pass additional configuration to telescope to change theme, layout, etc.
				require("telescope.builtin").current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
					winblend = 10,
					previewer = false,
				}))
			end, { desc = "[/] Fuzzily search in current buffer" })

			local function telescope_live_grep_open_files()
				require("telescope.builtin").live_grep({
					grep_open_files = true,
					prompt_title = "Live Grep in Open Files",
				})
			end
			vim.keymap.set("n", "<leader>s/", telescope_live_grep_open_files, { desc = "[S]earch [/] in Open Files" })
			vim.keymap.set(
				"n",
				"<leader>ss",
				require("telescope.builtin").builtin,
				{ desc = "[S]earch [S]elect Telescope" }
			)
			vim.keymap.set("n", "<leader>gf", require("telescope.builtin").git_files, { desc = "Search [G]it [F]iles" })
			vim.keymap.set("n", "<leader>sf", require("telescope.builtin").find_files, { desc = "[S]earch [F]iles" })
			vim.keymap.set("n", "<leader>sh", require("telescope.builtin").help_tags, { desc = "[S]earch [H]elp" })
			vim.keymap.set(
				"n",
				"<leader>sw",
				require("telescope.builtin").grep_string,
				{ desc = "[S]earch current [W]ord" }
			)
			vim.keymap.set("n", "<leader>sg", require("telescope.builtin").live_grep, { desc = "[S]earch by [G]rep" })
			vim.keymap.set("n", "<leader>sG", ":LiveGrepGitRoot<cr>", { desc = "[S]earch by [G]rep on Git Root" })
			vim.keymap.set(
				"n",
				"<leader>sd",
				require("telescope.builtin").diagnostics,
				{ desc = "[S]earch [D]iagnostics" }
			)
			vim.keymap.set("n", "<leader>sr", require("telescope.builtin").resume, { desc = "[S]earch [R]esume" })
		end,
	},
	{
		"stevearc/oil.nvim",
		opts = {},
		-- Optional dependencies
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })
			require("oil").setup({
				default_file_explorer = true,
				columns = {
					"icon",
					-- "permissions",
					-- "size",
					-- "mtime",
				},
				buf_options = {
					buflisted = false,
					bufhidden = "hide",
				},
				win_options = {
					wrap = false,
					signcolumn = "no",
					cursorcolumn = false,
					foldcolumn = "0",
					spell = false,
					list = false,
					conceallevel = 3,
					concealcursor = "nvic",
				},
				-- Send deleted files to the trash instead of permanently deleting them (:help oil-trash)
				delete_to_trash = false,
				-- Skip the confirmation popup for simple operations
				skip_confirm_for_simple_edits = false,
				-- Selecting a new/moved/renamed file or directory will prompt you to save changes first
				prompt_save_on_select_new_entry = true,
				-- Oil will automatically delete hidden buffers after this delay
				-- You can set the delay to false to disable cleanup entirely
				-- Note that the cleanup process only starts when none of the oil buffers are currently displayed
				cleanup_delay_ms = 2000,
				lsp_file_methods = {
					-- Time to wait for LSP file operations to complete before skipping
					timeout_ms = 1000,
					-- Set to true to autosave buffers that are updated with LSP willRenameFiles
					-- Set to "unmodified" to only save unmodified buffers
					autosave_changes = false,
				},
				-- Constrain the cursor to the editable parts of the oil buffer
				-- Set to `false` to disable, or "name" to keep it on the file names
				constrain_cursor = "editable",
				-- Keymaps in oil buffer. Can be any value that `vim.keymap.set` accepts OR a table of keymap
				-- options with a `callback` (e.g. { callback = function() ... end, desc = "", mode = "n" })
				-- Additionally, if it is a string that matches "actions.<name>",
				-- it will use the mapping at require("oil.actions").<name>
				-- Set to `false` to remove a keymap
				-- See :help oil-actions for a list of all available actions
				keymaps = {
					["g?"] = "actions.show_help",
					["<CR>"] = "actions.select",
					["<C-s>"] = "actions.select_vsplit",
					["<C-h>"] = "actions.select_split",
					["<C-t>"] = "actions.select_tab",
					["<C-p>"] = "actions.preview",
					["<C-c>"] = "actions.close",
					["<C-l>"] = "actions.refresh",
					["-"] = "actions.parent",
					["_"] = "actions.open_cwd",
					["`"] = "actions.cd",
					["~"] = "actions.tcd",
					["gs"] = "actions.change_sort",
					["gx"] = "actions.open_external",
					["g."] = "actions.toggle_hidden",
					["g\\"] = "actions.toggle_trash",
				},
				-- Configuration for the floating keymaps help window
				keymaps_help = {
					border = "rounded",
				},
				-- Set to false to disable all of the above keymaps
				use_default_keymaps = true,
				view_options = {
					-- Show files and directories that start with "."
					show_hidden = false,
					-- This function defines what is considered a "hidden" file
					is_hidden_file = function(name, bufnr)
						return vim.startswith(name, ".")
					end,
					-- This function defines what will never be shown, even when `show_hidden` is set
					is_always_hidden = function(name, bufnr)
						return false
					end,
					sort = {
						-- sort order can be "asc" or "desc"
						-- see :help oil-columns to see which columns are sortable
						{ "type", "asc" },
						{ "name", "asc" },
					},
				},
				-- Configuration for the floating window in oil.open_float
				float = {
					-- Padding around the floating window
					padding = 2,
					max_width = 0,
					max_height = 0,
					border = "rounded",
					win_options = {
						winblend = 0,
					},
					-- This is the config that will be passed to nvim_open_win.
					-- Change values here to customize the layout
					override = function(conf)
						return conf
					end,
				},
				-- Configuration for the actions floating preview window
				preview = {
					-- Width dimensions can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
					-- min_width and max_width can be a single value or a list of mixed integer/float types.
					-- max_width = {100, 0.8} means "the lesser of 100 columns or 80% of total"
					max_width = 0.9,
					-- min_width = {40, 0.4} means "the greater of 40 columns or 40% of total"
					min_width = { 40, 0.4 },
					-- optionally define an integer/float for the exact width of the preview window
					width = nil,
					-- Height dimensions can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
					-- min_height and max_height can be a single value or a list of mixed integer/float types.
					-- max_height = {80, 0.9} means "the lesser of 80 columns or 90% of total"
					max_height = 0.9,
					-- min_height = {5, 0.1} means "the greater of 5 columns or 10% of total"
					min_height = { 5, 0.1 },
					-- optionally define an integer/float for the exact height of the preview window
					height = nil,
					border = "rounded",
					win_options = {
						winblend = 0,
					},
					-- Whether the preview window is automatically updated when the cursor is moved
					update_on_cursor_moved = true,
				},
				-- Configuration for the floating progress window
				progress = {
					max_width = 0.9,
					min_width = { 40, 0.4 },
					width = nil,
					max_height = { 10, 0.9 },
					min_height = { 5, 0.1 },
					height = nil,
					border = "rounded",
					minimized_border = "none",
					win_options = {
						winblend = 0,
					},
				},
				ssh = {
					border = "rounded",
				},
			})
		end,
	},
	{
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v3.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
			-- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
		},
		config = function()
			vim.fn.sign_define("DiagnosticSignError", { text = " ", texthl = "DiagnosticSignError" })
			vim.fn.sign_define("DiagnosticSignWarn", { text = " ", texthl = "DiagnosticSignWarn" })
			vim.fn.sign_define("DiagnosticSignInfo", { text = " ", texthl = "DiagnosticSignInfo" })
			vim.fn.sign_define("DiagnosticSignHint", { text = "󰌵", texthl = "DiagnosticSignHint" })
			require("neo-tree").setup({
				close_if_last_window = false, -- Close Neo-tree if it is the last window left in the tab
				popup_border_style = "rounded",
				enable_git_status = true,
				enable_diagnostics = true,
				enable_normal_mode_for_inputs = false,                 -- Enable normal mode for input dialogs.
				open_files_do_not_replace_types = { "terminal", "trouble", "qf" }, -- when opening files, do not use windows containing these filetypes or buftypes
				sort_case_insensitive = false,                         -- used when sorting files and directories in the tree
				sort_function = nil,                                   -- use a custom function for sorting files and directories in the tree
				-- sort_function = function (a,b)
				--       if a.type == b.type then
				--           return a.path > b.path
				--       else
				--           return a.type > b.type
				--       end
				--   end , -- this sorts files and directories descendantly
				default_component_configs = {
					container = {
						enable_character_fade = true,
					},
					indent = {
						indent_size = 2,
						padding = 1, -- extra padding on left hand side
						-- indent guides
						with_markers = true,
						indent_marker = "│",
						last_indent_marker = "└",
						highlight = "NeoTreeIndentMarker",
						-- expander config, needed for nesting files
						with_expanders = nil, -- if nil and file nesting is enabled, will enable expanders
						expander_collapsed = "",
						expander_expanded = "",
						expander_highlight = "NeoTreeExpander",
					},
					icon = {
						folder_closed = "",
						folder_open = "",
						folder_empty = "󰜌",
						-- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there
						-- then these will never be used.
						default = "*",
						highlight = "NeoTreeFileIcon",
					},
					modified = {
						symbol = "[+]",
						highlight = "NeoTreeModified",
					},
					name = {
						trailing_slash = false,
						use_git_status_colors = true,
						highlight = "NeoTreeFileName",
					},
					git_status = {
						symbols = {
							-- Change type
							added = "", -- or "✚", but this is redundant info if you use git_status_colors on the name
							modified = "", -- or "", but this is redundant info if you use git_status_colors on the name
							deleted = "✖", -- this can only be used in the git_status source
							renamed = "󰁕", -- this can only be used in the git_status source
							-- Status type
							untracked = "",
							ignored = "",
							unstaged = "󰄱",
							staged = "",
							conflict = "",
						},
					},
					-- If you don't want to use these columns, you can set `enabled = false` for each of them individually
					file_size = {
						enabled = true,
						required_width = 64, -- min width of window required to show this column
					},
					type = {
						enabled = true,
						required_width = 122, -- min width of window required to show this column
					},
					last_modified = {
						enabled = true,
						required_width = 88, -- min width of window required to show this column
					},
					created = {
						enabled = true,
						required_width = 110, -- min width of window required to show this column
					},
					symlink_target = {
						enabled = false,
					},
				},
				-- A list of functions, each representing a global custom command
				-- that will be available in all sources (if not overridden in `opts[source_name].commands`)
				-- see `:h neo-tree-custom-commands-global`
				commands = {},
				window = {
					position = "left",
					width = 40,
					mapping_options = {
						noremap = true,
						nowait = true,
					},
					mappings = {
						["<space>"] = {
							"toggle_node",
							nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use
						},
						["<2-LeftMouse>"] = "open",
						["<cr>"] = "open",
						["<esc>"] = "cancel", -- close preview or floating neo-tree window
						["P"] = { "toggle_preview", config = { use_float = true, use_image_nvim = true } },
						-- Read `# Preview Mode` for more information
						["l"] = "focus_preview",
						["S"] = "open_split",
						["s"] = "open_vsplit",
						-- ["S"] = "split_with_window_picker",
						-- ["s"] = "vsplit_with_window_picker",
						["t"] = "open_tabnew",
						-- ["<cr>"] = "open_drop",
						-- ["t"] = "open_tab_drop",
						["w"] = "open_with_window_picker",
						--["P"] = "toggle_preview", -- enter preview mode, which shows the current node without focusing
						["C"] = "close_node",
						-- ['C'] = 'close_all_subnodes',
						["z"] = "close_all_nodes",
						--["Z"] = "expand_all_nodes",
						["a"] = {
							"add",
							-- this command supports BASH style brace expansion ("x{a,b,c}" -> xa,xb,xc). see `:h neo-tree-file-actions` for details
							-- some commands may take optional config options, see `:h neo-tree-mappings` for details
							config = {
								show_path = "none", -- "none", "relative", "absolute"
							},
						},
						["A"] = "add_directory", -- also accepts the optional config.show_path option like "add". this also supports BASH style brace expansion.
						["d"] = "delete",
						["r"] = "rename",
						["y"] = "copy_to_clipboard",
						["x"] = "cut_to_clipboard",
						["p"] = "paste_from_clipboard",
						["c"] = "copy", -- takes text input for destination, also accepts the optional config.show_path option like "add":
						-- ["c"] = {
						--  "copy",
						--  config = {
						--    show_path = "none" -- "none", "relative", "absolute"
						--  }
						--}
						["m"] = "move", -- takes text input for destination, also accepts the optional config.show_path option like "add".
						["q"] = "close_window",
						["R"] = "refresh",
						["?"] = "show_help",
						["<"] = "prev_source",
						[">"] = "next_source",
						["i"] = "show_file_details",
					},
				},
				nesting_rules = {},
				filesystem = {
					filtered_items = {
						visible = false, -- when true, they will just be displayed differently than normal items
						hide_dotfiles = true,
						hide_gitignored = true,
						hide_hidden = true, -- only works on Windows for hidden files/directories
						hide_by_name = {
							--"node_modules"
						},
						hide_by_pattern = { -- uses glob style patterns
							--"*.meta",
							--"*/src/*/tsconfig.json",
						},
						always_show = { -- remains visible even if other settings would normally hide it
							--".gitignored",
						},
						never_show = { -- remains hidden even if visible is toggled to true, this overrides always_show
							--".DS_Store",
							--"thumbs.db"
						},
						never_show_by_pattern = { -- uses glob style patterns
							--".null-ls_*",
						},
					},
					follow_current_file = {
						enabled = false,     -- This will find and focus the file in the active buffer every time
						--               -- the current file is changed while the tree is open.
						leave_dirs_open = false, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
					},
					group_empty_dirs = false, -- when true, empty folders will be grouped together
					hijack_netrw_behavior = "open_default", -- netrw disabled, opening a directory opens neo-tree
					-- in whatever position is specified in window.position
					-- "open_current",  -- netrw disabled, opening a directory opens within the
					-- window like netrw would, regardless of window.position
					-- "disabled",    -- netrw left alone, neo-tree does not handle opening dirs
					use_libuv_file_watcher = false, -- This will use the OS level file watchers to detect changes
					-- instead of relying on nvim autocmd events.
					window = {
						mappings = {
							["<bs>"] = "navigate_up",
							["."] = "set_root",
							["H"] = "toggle_hidden",
							["/"] = "fuzzy_finder",
							["D"] = "fuzzy_finder_directory",
							["#"] = "fuzzy_sorter", -- fuzzy sorting using the fzy algorithm
							-- ["D"] = "fuzzy_sorter_directory",
							["f"] = "filter_on_submit",
							["<c-x>"] = "clear_filter",
							["[g"] = "prev_git_modified",
							["]g"] = "next_git_modified",
							["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
							["oc"] = { "order_by_created", nowait = false },
							["od"] = { "order_by_diagnostics", nowait = false },
							["og"] = { "order_by_git_status", nowait = false },
							["om"] = { "order_by_modified", nowait = false },
							["on"] = { "order_by_name", nowait = false },
							["os"] = { "order_by_size", nowait = false },
							["ot"] = { "order_by_type", nowait = false },
						},
						fuzzy_finder_mappings = { -- define keymaps for filter popup window in fuzzy_finder_mode
							["<down>"] = "move_cursor_down",
							["<C-n>"] = "move_cursor_down",
							["<up>"] = "move_cursor_up",
							["<C-p>"] = "move_cursor_up",
						},
					},

					commands = {}, -- Add a custom command or override a global one using the same function name
				},
				buffers = {
					follow_current_file = {
						enabled = true, -- This will find and focus the file in the active buffer every time
						--              -- the current file is changed while the tree is open.
						leave_dirs_open = false, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
					},
					group_empty_dirs = true, -- when true, empty folders will be grouped together
					show_unloaded = true,
					window = {
						mappings = {
							["bd"] = "buffer_delete",
							["<bs>"] = "navigate_up",
							["."] = "set_root",
							["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
							["oc"] = { "order_by_created", nowait = false },
							["od"] = { "order_by_diagnostics", nowait = false },
							["om"] = { "order_by_modified", nowait = false },
							["on"] = { "order_by_name", nowait = false },
							["os"] = { "order_by_size", nowait = false },
							["ot"] = { "order_by_type", nowait = false },
						},
					},
				},
				git_status = {
					window = {
						position = "float",
						mappings = {
							["A"] = "git_add_all",
							["gu"] = "git_unstage_file",
							["ga"] = "git_add_file",
							["gr"] = "git_revert_file",
							["gc"] = "git_commit",
							["gp"] = "git_push",
							["gg"] = "git_commit_and_push",
							["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
							["oc"] = { "order_by_created", nowait = false },
							["od"] = { "order_by_diagnostics", nowait = false },
							["om"] = { "order_by_modified", nowait = false },
							["on"] = { "order_by_name", nowait = false },
							["os"] = { "order_by_size", nowait = false },
							["ot"] = { "order_by_type", nowait = false },
						},
					},
				},
			})

			vim.cmd([[nnoremap \ :Neotree reveal<cr>]])
		end,
	},
	{
		"stevanmilic/nvim-lspimport",
		config = function()
			vim.keymap.set("n", "<leader>a", require("lspimport").import, { noremap = true })
		end,
	},
	{
		"rmagatti/auto-session",
		config = function()
			require("auto-session").setup({
				log_level = "error",
				auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
			})
		end,
	},
	{
		"amitds1997/remote-nvim.nvim",
		version = "*",                  -- Pin to GitHub releases
		dependencies = {
			"nvim-lua/plenary.nvim",    -- For standard functions
			"MunifTanjim/nui.nvim",     -- To build the plugin UI
			"nvim-telescope/telescope.nvim", -- For picking b/w different remote methods
		},
		config = true,
	}
	-- }}}
})
-- }}}

-- Neovim Options {{{
-- See `:help vim.o`

-- Set highlight on search
vim.o.hlsearch = true

-- Make line numbers default
vim.opt.number = true
vim.opt.relativenumber = true

-- Enable mouse mode
vim.o.mouse = "a"

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = "unnamedplus"
vim.o.tabstop = 4
vim.o.expandtab = false
vim.o.shiftwidth = 4
vim.o.wrap = false
vim.o.scrolloff = 999

vim.o.splitbelow = true
vim.o.splitright = true

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = "yes"

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menuone,noselect"

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

-- Support for template-based formatting
-- vim.filetype.add({
-- 	pattern = {
-- 		["(.+).tmpl"] = function(path, bufnr, ext)
-- 			vim.print(vim.fn.fnamemodify(ext, ":e"))
-- 			vim.print(ext)
-- 			return vim.fn.fnamemodify(ext, ":e")
-- 		end,
-- 	},
-- })

function RunPrevCommandAndSwitchBack()
	local current_win = vim.api.nvim_get_current_win()
	print("current?")
	print(current_win)

	-- Switch to the opposite pane
	vim.api.nvim_command("wincmd w")

	-- Send 'Up' key to recall the previous command and 'Enter' to run it
	-- Adjust these keys if your terminal setup uses different keys
	vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("i", true, false, true), "t", false)
	vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Up>", true, false, true), "t", false)
	vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<CR>", true, false, true), "t", false)
	-- vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-\\>", true, false, true), "t", false)
	-- vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-N>", true, false, true), "t", false)
	-- print("setting back?")

	-- Switch back to the original pane
	vim.api.nvim_set_current_win(current_win)
end

-- Map <Leader><n> to the Lua function
nmap("<leader>n", RunPrevCommandAndSwitchBack, "Ru[n] command again in previous pane")

-- Highlight on yank {{{
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = highlight_group,
	pattern = "*",
})
-- }}}

-- }}}

-- Chezmoi template config {{{
require("config_templates")
-- }}}

-- Basic Keymaps {{{
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ "n", "v" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Diagnostic keymaps
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })

-- Terraform keymaps
-- vim.keymap.set("n", "<leader>ti", ":!terraform init<CR>", {)
-- vim.keymap.set("n", "<leader>tv", ":!terraform validate<CR>", opts)
-- vim.keymap.set("n", "<leader>tp", ":!terraform plan<CR>", opts)
-- vim.keymap.set("n", "<leader>taa", ":!terraform apply -auto-approve<CR>", opts)
-- }}}
