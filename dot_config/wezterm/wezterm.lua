local wezterm = require("wezterm")

local config = wezterm.config_builder()

local font_to_use = "SauceCodePro Nerd Font Mono"
config.font = wezterm.font(font_to_use)
config.freetype_load_flags = "NO_HINTING"
config.font_size = 15.0
config.custom_block_glyphs = false
config.font_rules = {
	{
		intensity = "Normal",
		italic = false,
		font = wezterm.font(font_to_use, { weight = "Medium", stretch = "Normal", style = "Normal" })
	},
	{
		intensity = "Bold",
		italic = false,
		font = wezterm.font(font_to_use, { weight = "Bold", stretch = "Normal", style = "Normal" }),
	},
	{
		intensity = "Bold",
		italic = true,
		font = wezterm.font(font_to_use, { weight = "Bold", stretch = "Normal", style = "Normal" }),
	},

-- When Intensity=Bold Italic=false:
-- wezterm.font_with_fallback({
--   -- /Users/martha/Library/Fonts/SauceCodeProNerdFontMono-SemiBold.ttf, CoreText
--   -- AKA: "SauceCodePro NFM"
--   -- AKA: "SauceCodePro NFM SemiBold"
--   {family="SauceCodePro Nerd Font Mono", weight="DemiBold"},
--
--   -- <built-in>, BuiltIn
--   "JetBrains Mono",
--
--   -- <built-in>, BuiltIn
--   -- Assumed to have Emoji Presentation
--   "Noto Color Emoji",
--
--   -- <built-in>, BuiltIn
--   "Symbols Nerd Font Mono",
--
-- })
--
--
-- When Intensity=Bold Italic=true:
-- wezterm.font_with_fallback({
--   -- /Users/martha/Library/Fonts/SauceCodeProNerdFontMono-SemiBoldItalic.ttf, CoreText
--   -- AKA: "SauceCodePro NFM"
--   -- AKA: "SauceCodePro NFM SemiBold"
--   {family="SauceCodePro Nerd Font Mono", weight="DemiBold", style="Italic"},
--
--   -- <built-in>, BuiltIn
--   "JetBrains Mono",
--
--   -- <built-in>, BuiltIn
--   -- Assumed to have Emoji Presentation
--   "Noto Color Emoji",
--
--   -- <built-in>, BuiltIn
--   "Symbols Nerd Font Mono",
--
-- })
}
config.enable_tab_bar = false
config.color_scheme = 'Github'

return config
